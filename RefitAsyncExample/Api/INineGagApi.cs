﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using RefitAsyncExample.Models;

namespace RefitAsyncExample.Api
{
    public interface INineGagApi
    {
        [Get("/api/Photos/getphotos")]
        Task<List<Photo>> GetPhotos();

        [Get("/api/Photos/getphoto/{id}")]
        Task<Photo> GetPhoto(int id);
    }
}