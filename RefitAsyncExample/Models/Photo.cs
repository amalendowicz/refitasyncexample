﻿using Newtonsoft.Json;

namespace RefitAsyncExample.Models
{
    public class Photo
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("photoUrl")]
        public string Url { get; set; }

        [JsonProperty("likes")]
        public int Likes { get; set; }

        [JsonProperty("dislikes")]
        public int Dislikes { get; set; }
    }
}