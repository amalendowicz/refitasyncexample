﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Refit;
using RefitAsyncExample.Api;
using RefitAsyncExample.Diagnostics;

namespace RefitAsyncExample
{
    class Program
    {
        private static INineGagApi _ninegagApi;

        static async Task Main(string[] args)
        {
            var httpClient = new HttpClient(new HttpClientDiagnosticsHandler(new HttpClientHandler()));
            httpClient.BaseAddress = new Uri("http://ilng.azurewebsites.net/");
            _ninegagApi = RestService.For<INineGagApi>(httpClient);


            Console.WriteLine("Pobieranie listy zdjęć");
            await GetPhotos();
            Console.WriteLine("Podaj id zdjęcia do pobrania danych");
            var id = Console.ReadLine();
            await GetPhotoData(Convert.ToInt32(id));
            Console.ReadKey();
        }

        private static async Task GetPhotoData(int id)
        {
            try
            {
                var photo = await _ninegagApi.GetPhoto(id);
                Console.WriteLine($"Url: {photo.Url}; Likes: {photo.Likes}; Dislikes: {photo.Dislikes}");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static async Task GetPhotos()
        {
            try
            {
                var photos = await _ninegagApi.GetPhotos();
                foreach (var photo in photos)
                {
                    Console.WriteLine(photo.Id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
